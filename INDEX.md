# DISPLAY

FreeDOS driver for codepage management (screen or printer)


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## DISPLAY.LSM

<table>
<tr><td>title</td><td>DISPLAY</td></tr>
<tr><td>version</td><td>0.13d</td></tr>
<tr><td>entered&nbsp;date</td><td>2018-01-02</td></tr>
<tr><td>description</td><td>FreeDOS driver for codepage management (screen or printer)</td></tr>
<tr><td>keywords</td><td>display, ega, vga, nls, cpi, cpx</td></tr>
<tr><td>author</td><td>Aitor Santamaria Merino</td></tr>
<tr><td>maintained&nbsp;by</td><td>Aitor Santamaria Merino</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.freedos.org</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
